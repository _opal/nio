# NIO server

Protocol consist of header and data, separated by 2 new lines<br />
Header: protocol name, version, length<br />
Content: the data<br />

Protocol name Fast Message Protocol (FMP)<br />
Version 1.0<br />

Example:<br />
FMP1.035

This is the content of the message

### Performance tests
To get first and last line in log<br />
grep -i DataAdded nio.log | sed -n '1p;$p'