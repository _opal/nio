#!/bin/bash

NUMBER_OF_CLIENTS=$1
TOTAL_ITER=$2

if [[ $NUMBER_OF_CLIENTS -eq 0 ]]
then
	NUMBER_OF_CLIENTS=1
fi
if [[ $TOTAL_ITER -eq 0 ]]
then
        TOTAL_ITER=1000
fi

echo "Starting $NUMBER_OF_CLIENTS clients..."
date

for ((i=0; i<$NUMBER_OF_CLIENTS; i++)) 
do
	java -cp target/nio-1.0-SNAPSHOT.jar com.opal.nio.ClientApplication -c custom/application.properties -i $TOTAL_ITER &
done

date
