rm /var/log/nio/nio.log

java -Xmx4G -XX:+AlwaysPreTouch -XX:+UseG1GC -XX:+ScavengeBeforeFullGC -XX:+DisableExplicitGC \
 -cp target/nio-1.0-SNAPSHOT.jar \
 com.opal.nio.NIOServerApplication \
 -c custom/application.properties
