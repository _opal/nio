package com.opal.nio;

import com.opal.nio.config.ApplicationConfig;
import com.opal.nio.server.Server;

import java.io.IOException;
import java.net.InetAddress;

public class NIOServerApplication {
    public static void main(String[] args) {
        try {
            if(args.length > 0) {
                Arguments.parseArgs(args);
            }
            ApplicationConfig config = ConfigurationFactory.makeApplicationConfiguration(Arguments.configFile);
            new Server(InetAddress.getLocalHost(), config.getPort(), config).start();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
