package com.opal.nio.handlers;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.concurrent.BlockingQueue;
import java.util.concurrent.TimeUnit;

public class MessageHandler implements Runnable {
    private static final Logger LOGGER = LoggerFactory.getLogger(MessageHandler.class);

    private final BlockingQueue<byte[]> queue;

    private boolean shouldRun = true;


    public MessageHandler(BlockingQueue<byte[]> queue) {
        this.queue = queue;
    }

    @Override
    public void run() {
        try {
            int i=0;
            while (shouldRun) {
                byte[] data = queue.poll(5000, TimeUnit.MILLISECONDS);

                if(null!=data) {
                    LOGGER.info("DataConsumed: i={}, data={}", i++, new String(data));
                }
            }
        } catch (Exception e) {
            LOGGER.error(e.getMessage());
        }
    }

    public void setShouldRun(boolean shouldRun) {
        this.shouldRun = shouldRun;
    }

    public BlockingQueue<byte[]> getQueue() {
        return queue;
    }
}
