package com.opal.nio;

import com.opal.nio.config.ApplicationConfig;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

public class ConfigurationFactory {
    public static ApplicationConfig makeApplicationConfiguration() {
        return makeApplicationConfiguration(null);
    }

    public static ApplicationConfig makeApplicationConfiguration(String configFile) {
        Properties properties = new Properties();
        if(null!=configFile) {
            System.out.println("Loading custom properties");
            try (InputStream is = new FileInputStream(configFile)) {
                properties.load(is);
                return new ApplicationConfig(properties);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        try (InputStream is = ClassLoader.getSystemClassLoader().getResourceAsStream("application.properties")) {
            properties.load(is);
            return new ApplicationConfig(properties);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return new ApplicationConfig();
    }
}
