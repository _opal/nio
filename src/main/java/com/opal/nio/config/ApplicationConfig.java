package com.opal.nio.config;

import java.util.Properties;

public class ApplicationConfig {
    private int tcpBufferSize = 1_000_000;

    private int readBufferSize = 4096;

    private int sendBufferSize = 4096;

    private int port;

    public ApplicationConfig() {}

    public int getTcpBufferSize() {
        return tcpBufferSize;
    }

    public int getReadBufferSize() {
        return readBufferSize;
    }

    public int getSendBufferSize() {
        return sendBufferSize;
    }

    public int getPort() {
        return port;
    }

    public ApplicationConfig(Properties properties) {
        this.port = Integer.parseInt(properties.getProperty("port", "8000"));
        this.tcpBufferSize = Integer.parseInt(properties.getProperty("tcpBufferSize", "1_000_000"));
        this.readBufferSize = Integer.parseInt(properties.getProperty("readBufferSize", "4096"));
        this.sendBufferSize = Integer.parseInt(properties.getProperty("sendBufferSize", "4096"));
    }
}
