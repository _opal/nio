package com.opal.nio.protocols;

import com.opal.nio.server.MessageReader;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.nio.BufferUnderflowException;
import java.nio.ByteBuffer;
import java.util.concurrent.BlockingQueue;

public class MessageReaderImpl implements MessageReader {
    private static final Logger LOGGER = LoggerFactory.getLogger(MessageReaderImpl.class);

    private final ByteBuffer readBuffer  = ByteBuffer.allocate((int) Math.pow(2, 20)); // 1Mb

    private static final int HEADER_SIZE = 10;

    private final byte[] protocol = new byte[3];

    private final byte[] version = new byte[3];

    private final int clientId;

    private byte[] data;

    private int partialDataRead;

    private int dataSize;

    private final BlockingQueue<byte[]> queue;

    private int totalHandled;

    private int drops;

    private int totalReceived;


    public MessageReaderImpl(BlockingQueue<byte[]> queue, int currentConnectedClients) {
        this.queue = queue;
        this.clientId = currentConnectedClients;
    }

    public void process(int numRead) {
        try {
            readBuffer.flip();

            while (readBuffer.hasRemaining()) {
                // handle partial data from previous message
                if(partialDataRead > 0) {
                    // handle when data is larger then numRead
                    int numToRead = Math.min(numRead, dataSize - partialDataRead);
                    if(numToRead > 0) {
                        readBuffer.get(data, partialDataRead, numToRead);
                        partialDataRead += numToRead;
                        LOGGER.debug("clientId={}, Added partial data: {}", clientId, this);
                    }

                    // now we assembled a full data out of multiple messages
                    if(dataSize == partialDataRead) {
                        queue.add(data);
                        totalHandled += dataSize;
                        partialDataRead = 0;
                        LOGGER.info("DataAdded: i={}, clientId: {}, Partial data accumulated successfully: {}", totalReceived++, clientId, this);
                    }
                    continue;
                } else {
                    // Start reading protocol
                    readBuffer.get(protocol);
                    if ('F' != protocol[0] && 'M' != protocol[1] && 'P' != protocol[2]) {
                        LOGGER.warn("clientId={}, Protocol not supported {}, {}", clientId, new String(protocol), this);
                        init();
                        return;
                    }
                    // Read version
                    readBuffer.get(version);
                    // Read size
                    dataSize = readBuffer.getInt();
                    totalHandled += HEADER_SIZE;
                    // Create data array
                    data = new byte[dataSize];
                }

                if (readBuffer.remaining() >= dataSize) {
                    // handle full message
                    readBuffer.get(data);
                    queue.add(data);
                    totalHandled += dataSize;
                    LOGGER.info("DataAdded: i={}, clientId: {}, Full message added: {}", totalReceived++, clientId, this);
                } else {
                    // handle partial message
                    partialDataRead = readBuffer.remaining();
                    readBuffer.get(data, 0, partialDataRead);
                    LOGGER.debug("clientId={}, First partial data added {}", clientId, this);
                }
            }

            if(readBuffer.position() == readBuffer.limit()) {
                init();
            }
        } catch (IndexOutOfBoundsException | BufferUnderflowException e) {
            LOGGER.trace("Exception", e);
            init();
            LOGGER.warn("clientId={}, Total drops: {}, buffer size: {}", clientId, ++drops, readBuffer.limit());
        }
    }

    @Override
    public ByteBuffer getBuffer() {
        return readBuffer;
    }

    private void init() {
        totalHandled = 0;
        readBuffer.clear();
    }

    @Override
    public String toString() {
        return "MessageReaderImpl{" +
                "readBuffer=" + readBuffer +
                ", partialDataRead=" + partialDataRead +
                ", dataSize=" + dataSize +
                ", totalHandled=" + totalHandled +
                ", drops=" + drops +
                '}';
    }
}
