package com.opal.nio.protocols;

import java.util.Arrays;

public class MessagesBuffer {
    private byte messages[][];

    private int position;


    public MessagesBuffer(int bufferSize) {
        this.messages = new byte [bufferSize][];
    }

    public boolean add(byte[] message) {
        if(position < message.length) {
            this.messages[position] = message;
            return true;
        }
        return false;
    }

    public byte[] pop() {
        byte[] data = messages[position++];
        messages[position-1] = null;
        return data;
    }

    public byte[][] drain() {
        byte[][] data = messages;
        Arrays.stream(messages).forEach(message -> message = null);
        return data;
    }
}
