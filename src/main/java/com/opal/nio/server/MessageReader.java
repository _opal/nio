package com.opal.nio.server;

import java.nio.ByteBuffer;

public interface MessageReader {
    void process(int numRead);

    ByteBuffer getBuffer();
}