package com.opal.nio.server;

import com.opal.nio.config.ApplicationConfig;
import com.opal.nio.handlers.MessageHandler;
import com.opal.nio.protocols.MessageReaderImpl;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.net.InetAddress;
import java.net.StandardSocketOptions;
import java.nio.channels.SelectionKey;
import java.nio.channels.Selector;
import java.nio.channels.ServerSocketChannel;
import java.nio.channels.SocketChannel;
import java.util.Iterator;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class Server extends Thread {
    private static final Logger LOGGER = LoggerFactory.getLogger(Server.class);

    private Selector selector;

    private volatile boolean shouldRun;

    private MessageHandler messageHandler;

    private ExecutorService executorService;

    private int currentConnectedClients;

    private ApplicationConfig config;


    public Server(InetAddress ip, int port, ApplicationConfig config) throws IOException {
        this(ip, port);
        this.config = config;
    }

    public Server(InetAddress ip, int port) throws IOException {
        selector = SelectorFactory.create(ip, port);
        // TODO: MessageHandler should be either a listener to events or use a setter
        messageHandler = new MessageHandler(new ArrayBlockingQueue<>(1000));
        executorService = Executors.newFixedThreadPool(4);
        executorService.submit(messageHandler);
    }

    public void run() {
        shouldRun = true;

        while (shouldRun) {
            try {
                // Wait for an event one of the registered channels
                this.selector.select();

                // Iterate over the set of keys for which events are available
                Iterator<SelectionKey> selectedKeys = this.selector.selectedKeys().iterator();
                while (selectedKeys.hasNext()) {
                    SelectionKey key = selectedKeys.next();

                    if (!key.isValid()) {
                        continue;
                    }

                    // Check what event is available and deal with it
                    if (key.isAcceptable()) {
                        System.out.println("New connection established");
                        this.accept(key);
                        currentConnectedClients++;
                    } else if (key.isReadable()) {
                        System.out.println("Reading data");
                        this.read(key);
                    }

                    selectedKeys.remove();
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    private void accept(SelectionKey key) throws IOException {
        // For accept to be pending the channel must be a server socket channel.
        ServerSocketChannel serverSocketChannel = (ServerSocketChannel) key.channel();
        serverSocketChannel.socket().setReceiveBufferSize((int) Math.pow(2, 24));

        // Accept the connection and make it non-blocking
        SocketChannel socketChannel = serverSocketChannel.accept();
        socketChannel.configureBlocking(false);
        socketChannel.setOption(StandardSocketOptions.SO_RCVBUF, config.getReadBufferSize());

        // Register the new SocketChannel with our Selector, indicating
        // we'd like to be notified when there's data waiting to be read
        // TODO: MessageReaderImpl should be provided by a provider and SHOULD NOT be instantiated here
        socketChannel.register(this.selector, SelectionKey.OP_READ, new MessageReaderImpl(messageHandler.getQueue(), currentConnectedClients));
    }

    private void read(SelectionKey key) throws IOException {
        SocketChannel socketChannel = (SocketChannel) key.channel();

        if(null != socketChannel) {
            MessageReader messageReader = (MessageReader) key.attachment();
            int numRead = socketChannel.read(messageReader.getBuffer());
            if(-1 == numRead) {
                // The remote forcibly closed the connection, cancel
                // the selection key and close the channel.
                key.cancel();
                socketChannel.close();
                System.out.println("Connection closed by remote");
                currentConnectedClients--;
            }
            else if (numRead > 0) {
                // Hand the data off to our worker thread
                messageReader.process(numRead);
            }
            else {
                LOGGER.warn("Nothing was read");
            }
        }
    }

    public void stopServer() {
        shouldRun = false;
    }
}
