package com.opal.nio;

public class Arguments {
    public static String configFile;
    public static int totalIterations = 1_000;
    public static int messageLength = 1_000;

    public static void parseArgs(String[] args) {
        if(args.length%2 != 0) return;

        for(int i=0; i< args.length; i+=2) {
            switch (args[i]) {
                case "-c":
                    configFile = args[i+1];
                    break;
                case "-i":
                    totalIterations = Integer.parseInt(args[i+1]);
                    break;
                case "-l":
                    messageLength = Integer.parseInt(args[i+1]);
                    break;
            }
        }

        System.out.format("Config: %s, Iter: %d, message length: %d\n",
                Arguments.configFile,
                Arguments.totalIterations,
                Arguments.messageLength);
    }
}
