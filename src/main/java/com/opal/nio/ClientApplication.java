package com.opal.nio;

import com.opal.nio.config.ApplicationConfig;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.net.InetAddress;
import java.net.InetSocketAddress;
import java.net.SocketOption;
import java.net.SocketOptions;
import java.nio.ByteBuffer;
import java.nio.channels.SocketChannel;
import java.util.Random;

public class ClientApplication {
    private static final Logger LOGGER = LoggerFactory.getLogger(ClientApplication.class);

    private final ApplicationConfig config;


    public ClientApplication() {
        config = ConfigurationFactory.makeApplicationConfiguration(Arguments.configFile);
    }

    public static void main(String[] args) throws IOException, InterruptedException {
        if(args.length > 0) {
            Arguments.parseArgs(args);
        }
        new ClientApplication().run();
    }

    private void run() throws IOException, InterruptedException {
        SocketChannel socketChannel = initiateConnection();
        String header = "FMC1.0";

        int i = 0;
        ByteBuffer writeBuffer =
                ByteBuffer.allocate(Arguments.totalIterations * Arguments.messageLength + header.length() + 4);
        writeBuffer.clear();

        while (i++ < Arguments.totalIterations) {
            String data = createRandomString((int) (Math.random()*Arguments.messageLength));
            data = String.format("Message %d: %s", i, data);
            LOGGER.info("Message {}, size: {}", i, data.length());
            int size = data.getBytes().length;
            writeBuffer.put(header.getBytes())
                .putInt(size)
                .put(data.getBytes());
        }

        writeBuffer.flip();

        LOGGER.info("Start sending: {}, position: {}", writeBuffer.remaining(), writeBuffer.position());

        while (writeBuffer.hasRemaining()) {
            socketChannel.write(writeBuffer);
            LOGGER.info("Sent {} bytes, remaining: {}", writeBuffer.position(), writeBuffer.remaining());
//            Thread.sleep(10);
        }

        Thread.sleep(2000);
        socketChannel.close();
    }

    private String createRandomString(int targetStringLength) {
        int leftLimit = 48; // numeral '0'
        int rightLimit = 122; // letter 'z'
        Random random = new Random();

        return random.ints(leftLimit, rightLimit + 1)
                .filter(i -> (i <= 57 || i >= 65) && (i <= 90 || i >= 97) && i!=20)
                .limit(targetStringLength)
                .collect(StringBuilder::new, StringBuilder::appendCodePoint, StringBuilder::append)
                .toString();
    }

    private SocketChannel initiateConnection() throws IOException {
        // Create a non-blocking socket channel
        SocketChannel socketChannel = SocketChannel.open();
        socketChannel.configureBlocking(false);
        socketChannel.socket().setSendBufferSize(config.getSendBufferSize());
        socketChannel.connect(new InetSocketAddress(InetAddress.getLocalHost(), config.getPort()));
        while (!socketChannel.finishConnect());
        LOGGER.info("Connection created on port: {}, buffer size: {}", config.getPort(), config.getSendBufferSize());
        return socketChannel;
    }
}
